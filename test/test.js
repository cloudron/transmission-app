#!/usr/bin/env node

/* jslint node:true */
/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    fs = require('fs'),
    expect = require('expect.js'),
    path = require('path'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

    const username = process.env.TEST_USERNAME || process.env.USERNAME;
const password = process.env.TEST_PASSWORD || process.env.PASSWORD;

if (!username || !password) {
    console.log('USERNAME and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    let browser, app;

    const LOCATION = process.env.LOCATION || 'test';
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 50000;

    const torrentUrl = 'https://git.cloudron.io/cloudron/transmission-app/-/raw/master/test/ubuntu-22.10-desktop-amd64.iso.torrent?inline=false';

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(async function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    async function getAppInfo() {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(a => a.location === LOCATION || a.location === LOCATION + '2')[0];
        expect(app).to.be.an('object');
    }

    async function login(alreadyAuthenticated=true) {
        await browser.get(`https://${app.fqdn}/login`);

        await browser.wait(until.elementLocated(By.id('loginProceedButton')));
        await browser.findElement(By.id('loginProceedButton')).click();
        if (!alreadyAuthenticated) {
            await browser.wait(until.elementLocated(By.xpath('//input[@name="username"]')), 5000);
            await browser.findElement(By.xpath('//input[@name="username"]')).sendKeys(username);
            await browser.findElement(By.xpath('//input[@name="password"]')).sendKeys(password);
            await browser.sleep(2000);
            await browser.findElement(By.id('loginSubmitButton')).click();
        }
        await browser.wait(until.elementLocated(By.xpath('//*[@id="torrent-container"]')), TIMEOUT);
        await browser.sleep(2000);
    }

    async function logout() {
        await browser.get(`https://${app.fqdn}/logout`);
        await browser.sleep(2000); // wait for "connection lost" error
        await browser.get(`https://${app.fqdn}/login`);
        await browser.wait(until.elementLocated(By.id('loginProceedButton')));
    }

    async function canAddTorrent() {
        await browser.findElement(By.id('toolbar-open'), TIMEOUT).click();
        await browser.findElement(By.xpath('//input[@type="url"]'), TIMEOUT).sendKeys(torrentUrl);
        await browser.findElement(By.xpath('//button[contains(text(), "Add")]'), TIMEOUT).click();
    }

    async function isTorrentInList() {
        await browser.wait(until.elementLocated(By.xpath('//div[contains(@class, "torrent-name")]')), TIMEOUT);
        const element = await browser.findElement(By.xpath('//div[contains(@class, "torrent-name")]'), TIMEOUT);
        const text = await element.getText();
        expect(text).to.contain('ubuntu');
    }

    xit('build app', () => { execSync('cloudron build', EXEC_ARGS); });

    it('install app', () => { execSync(`cloudron install --location ${LOCATION} -p TORRENT_PORT=51416`, EXEC_ARGS); });

    it('can get app information', getAppInfo);

    it('can login', login.bind(null, false));
    it('can add torrent', canAddTorrent);
    it('is torrent in list', isTorrentInList);
    it('can logout', logout);

    it('can restart app', () => { execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS); });
    it('can login', login);
    it('is torrent in list', isTorrentInList);
    it('can logout', logout);

    it('backup app', () => { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });
    it('restore app', () => {
        const backups = JSON.parse(execSync('cloudron backup list --raw'));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --location ' + LOCATION + ' -p TORRENT_PORT=51416', EXEC_ARGS);
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(a => a.location === LOCATION)[0];
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can login', login);
    it('is torrent in list', isTorrentInList);
    it('can logout', logout);

    it('move to different location', () => { execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS); });
    it('can get app information', getAppInfo);
    it('can login', login);
    it('is torrent in list', isTorrentInList);
    it('can logout', logout);

    it('uninstall app', () => { execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS); });

    // test update
    it('can install app for update', () => { execSync(`cloudron install --appstore-id com.transmissionbt.cloudronapp --location ${LOCATION} -p TORRENT_PORT=51416`, EXEC_ARGS); });
    it('can get app information', getAppInfo);
    it('can login', login);
    it('can add torrent', canAddTorrent);
    it('can update', () => { execSync(`cloudron update --app ${app.id}`, EXEC_ARGS); });
    it('can login', login);
    it('is torrent in list', isTorrentInList);
    it('can logout', logout);
    it('uninstall app', () => { execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS); });
});
