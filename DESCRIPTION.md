### Overview

Transmission is a cross-platform BitTorrent client.

### Features

* Uses fewer resources than other clients
* Daemon ideal for servers, embedded systems, and headless use
* All these can be remote controlled by Web and Terminal clients
* Local Peer Discovery
* Full encryption, DHT, µTP, PEX and Magnet Link support

