#!/usr/bin/env node
'use strict'

const fs = require('fs')
const path = require('path')
const childProcess = require('child_process')
const { promisify } = require('util')

const execFile = promisify(childProcess.execFile)

const transmissionConfig = JSON.parse(fs.readFileSync('/app/data/config/settings.json', 'utf-8'))
const transmissionDownloadingDir = transmissionConfig['download-dir']

const processFile = async (inputPath, downloadingDir, downloadedDir) => {
  const outputPath = inputPath.replace(downloadingDir, downloadedDir)
  const outputDir = path.dirname(outputPath)
  await fs.promises.mkdir(outputDir, { recursive: true })
  await fs.promises.link(inputPath, outputPath)
}

const processPath = async (toProcess, downloadingDir, downloadedDir) => {
  const result = await fs.promises.stat(toProcess)

  if (result.isDirectory()) {
    const dirList = await fs.promises.readdir(toProcess)
    for (const entry of dirList) {
      await processPath(path.join(toProcess, entry), downloadingDir, downloadedDir)
    }
  } else if (result.isFile()) {
    await processFile(toProcess, downloadingDir, downloadedDir)
  } // else, it's a weird thing, ignore it
}

const torrentDir = process.env.TR_TORRENT_DIR
const torrentName = process.env.TR_TORRENT_NAME

if (!torrentDir || !torrentName) {
  console.error('Error: you must call this script from transmission. TR_TORRENT_DIR & TR_TORRENT_NAME were not passed')
  process.exit(1)
}

const torrentPath = path.join(torrentDir, torrentName)

if (torrentPath.startsWith(transmissionDownloadingDir) && transmissionDownloadingDir.endsWith('/Downloading/')) {
  const downloadedDir = transmissionDownloadingDir.replace(/\/Downloading\/$/, '/Downloaded/')
  processPath(torrentPath, transmissionDownloadingDir, downloadedDir)
    .then(() => execFile('/app/code/transmission/purge.sh'))
} else {
  execFile('/app/code/transmission/purge.sh')
}
