FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN apt-get update -y && \
    apt-get install -y build-essential automake autoconf libtool pkg-config intltool libcurl4-openssl-dev libminiupnpc-dev libevent-dev locales && \
    rm -rf /var/cache/apt /var/lib/apt/lists

RUN mkdir -p /app/code/transmission
WORKDIR /app/code

# renovate: datasource=github-releases depName=transmission/transmission versioning=semver
ARG TRANSMISSION_VERSION=4.0.6

RUN curl -L "https://github.com/transmission/transmission/releases/download/${TRANSMISSION_VERSION}/transmission-${TRANSMISSION_VERSION}.tar.xz" \
        | tar -x --xz --strip-components=1 -C /app/code/transmission && \
    cd transmission && \
    mkdir build && \
    cd build && \
    cmake -DCMAKE_BUILD_TYPE=RelWithDebInfo -DENABLE_TESTS=OFF .. && \
    make -j4 && \
    make install && \
    rm -rf /app/code/transmission

RUN npm install -g json

COPY start.sh /app/code/
COPY transmission /app/code/transmission

CMD [ "/app/code/start.sh" ]
