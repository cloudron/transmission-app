#!/bin/bash

set -eu

# https://github.com/transmission/transmission/blob/main/docs/Configuration-Files.md has structure of config
mkdir -p /app/data/config/ /app/data/downloads

if [[ ! -f /app/data/config/settings.json ]]; then
    echo "=> Copying settings file on first run"
    cp /app/code/transmission/settings.json.template /app/data/config/settings.json
fi

# https://github.com/transmission/transmission/blob/main/docs/Editing-Configuration-Files.md
json -I -f /app/data/config/settings.json \
    -e "this['peer-port'] = ${TORRENT_PORT:-51413}" \
    -e "this['rpc-host-whitelist'] = '${CLOUDRON_APP_DOMAIN:-localhost}'" \
    -e "this['rpc-authentication-required'] = false" \
    -e "this['rpc-port'] = 9091" \
    -e "this['rpc-whitelist-enabled'] = false"

chown -R cloudron:cloudron /app/data

echo "Starting Transmission..."
/usr/local/bin/gosu cloudron:cloudron /usr/local/bin/transmission-daemon --config-dir /app/data/config/ --foreground
